const express = require('express');
const app = express();
const cors = require('cors');
const port = process.env.PORT || 5001;

/* Including all routes */
const subscriptions = require('./routes/subscriptions');
const users = require('./routes/users');

require('dotenv').config()

if (!process.env.AUTH0_DOMAIN || !process.env.AUTH0_AUDIENCE) {
    throw 'Make sure you have AUTH0_DOMAIN, and AUTH0_AUDIENCE in your .env file';
}

const corsOptions = { origin: 'http://localhost:' + port };
app.use(cors(corsOptions));

app.use(express.json());
app.use('/api/subscriptions', subscriptions);
app.use('/api/users', users);

app.use(function (err, req, res, next) {
    console.error(err.stack);
    return res.status(err.status).json({
        message: err.message
    });
});

app.listen(port, () => console.log(`Listening on port http://localhost:${port}......`));