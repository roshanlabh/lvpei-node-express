module.exports = function (sequelize, DataTypes) {
  return sequelize.define('subscription_user', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    subscription_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    user_id: {
      type: DataTypes.STRING(125),
      allowNull: false
    }
  }, {
    tableName: 'subscription_user'
  });
};