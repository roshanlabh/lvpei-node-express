module.exports = function (sequelize, DataTypes) {
    return sequelize.define('users', {
        id: {
            type: DataTypes.STRING(125),
            allowNull: false,
            primaryKey: true
        },
        fname: {
            type: DataTypes.STRING(255),
            allowNull: false
        },
        lname: {
            type: DataTypes.STRING(255),
            allowNull: false
        },
        email: {
            type: DataTypes.STRING(255),
            allowNull: false
        },
        role_id: {
            type: DataTypes.INTEGER(10),
            allowNull: false
        },
        auth0_response: {
            type: DataTypes.STRING(500),
            allowNull: true
        }
    }, {
        tableName: 'users'
    });
};