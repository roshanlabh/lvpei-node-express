const Joi = require('joi');
const request = require('request');
const db = require('../config/dbconnection');
const Subscription = require('../models/subscription')(db.connection, db.Sequelize);
const SubscriptionUser = require('../models/subscription_user')(db.connection, db.Sequelize);

const express = require('express');
const router = express.Router();

/***
 * Create New Subscription(Client)
 * Endpoint: /api/subscriptions
 * Method: POST
 * Return: Subscription ID (JSON) on success
 */
router.post('/', async (req, res) => {
    //Validation Schema
    const schema = Joi.object().keys({
        name: Joi.string().max(255).required(),
        start_date: Joi.date().required(),
        end_date: Joi.date().required(),
        address1: Joi.string().min(5).required(),
        address2: Joi.string().min(5).required(),
        city: Joi.string().min(5).required(),
        state: Joi.string().min(1).required(),
        pin: Joi.number().min(5).required(),
        telephone: Joi.string().required(),
        subscription_type: Joi.number().required()
    });

    //Validating the Input
    const result = Joi.validate(req.body, schema);
    if (result.error) {
        return res.status(422).send(result.error.details[0].message);
    }

    const subscription = Subscription.build({
        name: req.body.name,
        start_date: req.body.start_date,
        end_date: req.body.end_date,
        address1: req.body.address1,
        address2: req.body.address2,
        city: req.body.city,
        state: req.body.state,
        pin: req.body.pin,
        telephone: req.body.telephone,
        subscription_type: req.body.subscription_type
    });

    //Creating subscription
    subscription.save().then(function (result) {
        let response = {
            client_id: result.dataValues.id
        }
        return res.status(200).send(JSON.stringify(response));
    }).catch(function (err) {
        console.log("Error while inserting Subscription===", err);
        return res.status(500).send('Error in Inserting Record');
    }).done();
});

/*** Get all Subscriptions(Clients) details
 * Endpoint: /api/subscriptions
 * Method: GET
 * Return: All Subscriptions(JSON Array) on success
 */
router.get('/', (req, res) => {
    Subscription.findAll({
        where: {
            is_delete: 0,
        },
    }).then(function (result) {
        let response = [];
        result.forEach(element => {
            response.push(element.dataValues);
        });
        return res.status(200).send(JSON.stringify(response));
    }).catch(function (err) {
        console.log("Error while getting Subscription === ", err);
        return res.status(500).send('Error while getting result');
    }).done();
});

/*** Get a particular Subscription(Client) details
 * Endpoint: /api/subscriptions
 * Method: GET
 * Return: Particular Subscription(JSON) on success
 */
router.get('/:id', async (req, res) => {
    if (!req.params.id || req.params.id == "" || req.params.id == null) {
        return res.status(400).send("Invalid ID Provided");
    }
    Subscription.find({
        where: {
            id: req.params.id,
            is_delete: 0,
        },
    }).then(function (result) {
        return res.status(200).send(JSON.stringify(result));
    }).catch(function (err) {
        console.log("Error while getting Subscription===", err);
        return res.status(500).send('Error while getting result');
    }).done();
});

/*** Delete(Soft Delete) a particular Subscription(Client)
 * Endpoint: /api/subscriptions
 * Method: DELETE
 * Return: Success Message (String) on success
 */
router.delete('/:id', async (req, res) => {
    if (!req.params.id || req.params.id == "" || req.params.id == null) {
        return res.status(400).send("Invalid ID Provided");
    }

    Subscription.update({
        is_delete: 1
    }, {
        where: {
            id: req.params.id,
        },
    }).then(function (result) {
        if (result > 0) {
            res.status(200).send("Deleted Successfully");
        } else {
            res.status(200).send("Subscription not found");
        }
        return;
    }).catch(function (err) {
        console.log("Error while deleting Subscription===", err);
        return res.status(500).send('Error while delting subscription result');
    }).done();
});

/*** Update a particular Subscription(Client)
 * Endpoint: /api/subscriptions
 * Method: PUT
 * Return: Success Message (String) on success
 */
router.put('/:id', async (req, res) => {
    if (!req.params.id || req.params.id == "" || req.params.id == null) {
        return res.status(400).send("Invalid ID Provided");
    }

    //Validation Schema
    const schema = Joi.object().keys({
        name: Joi.string().max(255).required(),
        start_date: Joi.date().required(),
        end_date: Joi.date().required(),
        address1: Joi.string().min(5).required(),
        address2: Joi.string().min(5).required(),
        city: Joi.string().min(5).required(),
        state: Joi.string().min(1).required(),
        pin: Joi.number().min(5).required(),
        telephone: Joi.string().required(),
        subscription_type: Joi.number().required()
    });

    //Validating the Input
    const result = Joi.validate(req.body, schema);
    if (result.error) {
        return res.status(422).send(result.error.details[0].message);
    }

    //Creating subscription
    Subscription.update({
        name: req.body.name,
        start_date: req.body.start_date,
        end_date: req.body.end_date,
        address1: req.body.address1,
        address2: req.body.address2,
        city: req.body.city,
        state: req.body.state,
        pin: req.body.pin,
        telephone: req.body.telephone,
        subscription_type: req.body.subscription_type
    }, {
        where: {
            id: req.params.id,
        },
    }).then(function (result) {
        if (result > 0) {
            res.status(200).send("Updated Successfully");
        } else {
            res.status(200).send("Either Subscription was not found or nothing there to update.");
        }
        return;
    }).catch(function (err) {
        console.log("Error while updating Subscription===", err);
        return res.status(500).send('Error while Updating subscription result');
    }).done();
});


/*** Create a User(Admin) of Subscription(Client)
 * Endpoint: /api/subscriptions/createuser
 * Method: POST
 * Return: User ID (String) on success
 */
router.post('/createuser', async (req, res) => {
    //Validation Schema
    const schema = Joi.object().keys({
        fname: Joi.string().max(200).required(),
        lname: Joi.string().max(200).required(),
        email: Joi.string().max(200).email({ minDomainAtoms: 2 }),
        password: Joi.string().min(5).required(),
        role_id: Joi.number().required(),
        subscription_id: Joi.number().required()
    });

    //Validating the Input
    const result = Joi.validate(req.body, schema);
    if (result.error) {
        return res.status(400).send(result.error.details[0].message);
    }

    let options = {
        method: 'POST',
        url: `${process.env.USER_SERVICE_URI}:${process.env.USER_SERVICE_PORT}/api/users/register`,
        headers: { 'Content-Type': 'application/json' },
        body: {
            fname: req.body.fname,
            lname: req.body.lname,
            email: req.body.email,
            password: req.body.password,
            role_id: req.body.role_id
        },
        json: true
    };

    request(options, function (error, response, body) {
        console.log('createuser -> register -> body: ', body)
        if (error)
            return res.status(500).send({
                "message": "Some internal error. Please try again."
            });

        if (response.statusCode != 200) {
            return res.status(response.statusCode).send(body);
        }

        let subscription_user = SubscriptionUser.build({
            subscription_id: req.body.subscription_id,
            user_id: body._id
        });

        // Mapping User with Subscription
        subscription_user.save().then(function (result) {
            let response = {
                client_id: result.dataValues.id
            }
            return res.status(200).send(JSON.stringify(response));
        }).catch(function (err) {
            console.log("Error while inserting subscription_user=== ", err);
            return res.status(500).send('Error in Inserting Record');
        }).done();
    });
});

module.exports = router;