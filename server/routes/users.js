const express = require('express');
const router = express.Router();
const jwt = require('express-jwt');
const jwtAuthz = require('express-jwt-authz');
const jwksRsa = require('jwks-rsa');
const request = require('request');
const Joi = require('joi');

const db = require('../config/dbconnection');
const User = require('../models/user')(db.connection, db.Sequelize);

const checkJwt = jwt({
    // Dynamically provide a signing key based on the kid in the header and the singing keys provided by the JWKS endpoint.
    secret: jwksRsa.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: `https://${process.env.AUTH0_DOMAIN}/.well-known/jwks.json`
    }),

    // Validate the audience and the issuer.
    audience: process.env.AUTH0_AUDIENCE,
    issuer: `https://${process.env.AUTH0_DOMAIN}/`,
    algorithms: ['RS256']
});

const checkScopes = jwtAuthz(['read:messages']);

/** This are test API to test which api are public and protected */
router.get('/public', function (req, res) {
    res.json({
        message: 'Hello from a public endpoint! You don\'t need to be authenticated to see this.'
    });
});

router.get('/private', checkJwt, function (req, res) {
    console.log('\n \n Roshan\n');
    console.log('\n\n req.user: ', req.user);
    res.json({
        message: 'Hello from a private endpoint! You need to be authenticated to see this.'
    });
});

router.get('/private-scoped', checkJwt, checkScopes, function (req, res) {
    res.json({
        message: 'Hello from a private endpoint! You need to be authenticated and have a scope of read:messages to see this.'
    });
});
/** ---------- */

/*** Registering user on Auth0 server
 * Endpoint: /api/users/register
 * Method: POST
 * Params: JSON request as {"email": "test4@test.com", "password": "unify@123", "fname": "First", "lname": "Last", "role_id": 5 }
 * Return: user_id with complete json
 */
router.post('/register', function (req, res) {
    // Request validation
    const schema = {
        email: Joi.string().email({ minDomainAtoms: 2 }).required(),
        password: Joi.string().min(5).max(20).required(),
        fname: Joi.string().min(3),
        lname: Joi.string().min(3),
        role_id: Joi.number().integer()
    };

    const result = Joi.validate(req.body, schema);

    // If validation failed, return error with 400 status code.
    if (result.error) {
        return res.status(400).send({ "message": result.error.details[0].message });       //Bad request
    }

    let user_metadata = {};
    if (req.body.fname) user_metadata.fname = req.body.fname;
    if (req.body.lname) user_metadata.lname = req.body.lname;
    if (req.body.role_id) user_metadata.role_id = String(req.body.role_id);

    let options = {
        method: 'POST',
        url: `https://${process.env.AUTH0_DOMAIN}/dbconnections/signup`,
        headers: { 'content-type': 'application/json' },
        body: {
            client_id: process.env.AUTH0_CLIENT_ID,
            email: req.body.email,
            password: req.body.password,
            connection: process.env.AUTH0_DATABASE
        },
        json: true
    };

    if (user_metadata) options.body.user_metadata = user_metadata;

    request(options, function (error, response, body) {
        if (error)
            return res.status(500).send({ "message": "Some internal error. Please try again." });

        // Map fields and values for Inseration in users table
        let user = User.build({
            id: body._id,
            fname: req.body.fname,
            lname: req.body.lname,
            email: req.body.email,
            role_id: req.body.role_id,
            auth0_response: JSON.stringify(body)
        });

        // Saving into DB
        user.save().then(function (result) {
            return res.status(response.statusCode).send(body);
        }).catch(function (err) {
            console.log("Error while inserting user === ", err);
            return res.status(500).send('Error in Inserting Record');
        }).done();
    });
});

/*** Get Access Token from Auth0
 * Endpoint: /api/users/accesstoken
 * Method: GET
 * Return: Access Token on success
 */
router.get('/accesstoken', function (req, res) {
    let options = {
        method: 'POST',
        url: `https://${process.env.AUTH0_DOMAIN}/oauth/token`,
        headers: {
            'content-type': 'application/json'
        },
        body: {
            client_id: process.env.AUTH0_CLIENT_ID,
            client_secret: process.env.AUTH0_CLIENT_SECRET,
            audience: process.env.AUTH0_AUDIENCE,
            grant_type: 'client_credentials',
        },
        json: true
    };

    request(options, function (error, response, body) {
        if (error) {
            // throw new Error(error);
            return res.status(500).send({
                "message": "Some internal error. Please try again."
            });
        }

        let statusCode = response.statusCode;
        res.status(statusCode).send(body);
    });
});

/*** Get User's Access Token from Auth0
 * Endpoint: /api/users/usertoken
 * Method: POST
 * Return: Access Token on success
 */
router.post('/usertoken', function (req, res) {
    // Request validation
    const schema = {
        email: Joi.string().email({ minDomainAtoms: 2 }).required(),
        password: Joi.string().min(5).max(20).required()
    };

    const result = Joi.validate(req.body, schema);

    // If validation failed, return error with 400 status code.
    if (result.error)
        return res.status(400).send(result.error.details[0].message);   //Bad request

    let options = {
        method: 'POST',
        url: `https://${process.env.AUTH0_DOMAIN}/oauth/token`,
        headers: {
            'content-type': 'application/json'
        },
        body: {
            grant_type: 'http://auth0.com/oauth/grant-type/password-realm',
            username: req.body.email,
            password: req.body.password,
            audience: process.env.AUTH0_AUDIENCE,
            scope: 'openid profile email',
            client_id: process.env.AUTH0_CLIENT_ID,
            client_secret: process.env.AUTH0_CLIENT_SECRET,
            realm: 'Username-Password-Authentication'
        },
        json: true
    };

    request(options, function (error, response, body) {
        if (error)
            return res.status(500).send({
                "message": "Some internal error. Please try again."
            });
            
        let statusCode = response.statusCode;
        res.status(statusCode).send(body);
    });
});

module.exports = router;