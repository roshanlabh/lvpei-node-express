-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `subscriptions`;
CREATE TABLE `subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pin` int(10) NOT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subscription_type` tinyint(1) NOT NULL,
  `is_delete` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `subscriptions` (`id`, `name`, `start_date`, `end_date`, `address1`, `address2`, `city`, `state`, `pin`, `telephone`, `subscription_type`, `is_delete`) VALUES
(1,	'Titan Eye Plus',	'2018-10-01',	'2019-10-01',	'#1-98/3/31 PLOT NO-31 SRI SAI ARCADE BESIDE VODAFONE SHOWROOM BELOW K B S BANK MADHAPUR BRANCH MADHAPUR MAIN ROAD',	'VIP Hills, Jaihind Enclave, Madhapur',	'Hyderabad',	'TS',	500081,	'040 4024 8764',	1,	0),
(2,	'Vision Express',	'2018-09-01',	'2019-09-01',	'Ground Floor, Plot No.33, Hitech City Rd',	'Arunodaya Colony, Madhapur',	'Hyderabad',	'TS',	500081,	'040 4004 5273',	1,	0),
(3,	'Lenskart.com Eyewear Store Madhapur',	'2018-09-01',	'2019-09-01',	'S No. 64, Shop No. DK-1A, APIIC Software Layout Inorbit Mall',	'Food Court, L4, Mindspace, Madhapur',	'Hyderabad',	'TS',	500081,	'040 4854 9984',	1,	0),
(4,	'Lenskart.com Eyewear Store Kondapur',	'2018-09-01',	'2019-09-01',	'1-112/80, Opp. KIMS hospital',	'Kondapur',	'Hyderabad',	'TS',	500084,	'040 4018 9944',	1,	0),
(5,	'Optorium',	'2018-10-01',	'2019-09-30',	'Plot No. 6-3-906/A/1/B, Shiva Apartments',	'Lane Beside Yashoda Cancer Hospital',	'Hyderabad',	'TS',	500082,	'040 6666 5279',	1,	0);

DROP TABLE IF EXISTS `subscription_user`;
CREATE TABLE `subscription_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subscription_id` int(11) NOT NULL,
  `user_id` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `subscription_user` (`id`, `subscription_id`, `user_id`) VALUES
(1,	1,	'5b962da86a52865dc2e23bbf'),
(2,	2,	'5b96329872d4bb47f9a38dbf'),
(3,	5,	'5b963b776a52865dc2e23e83');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` varchar(125) CHARACTER SET latin1 NOT NULL COMMENT 'Auth0 user_id',
  `fname` varchar(255) CHARACTER SET latin1 NOT NULL COMMENT 'first name',
  `lname` varchar(255) CHARACTER SET latin1 NOT NULL COMMENT 'last name',
  `email` varchar(255) CHARACTER SET latin1 NOT NULL,
  `role_id` int(11) NOT NULL,
  `auth0_response` text CHARACTER SET latin1 NOT NULL COMMENT 'Auth0 complete response',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `users` (`id`, `fname`, `lname`, `email`, `role_id`, `auth0_response`) VALUES
('5b962da86a52865dc2e23bbf',	'Roshan',	'Labh',	'roshan@gmail.com',	1,	'{\"_id\":\"5b962da86a52865dc2e23bbf\",\"email_verified\":false,\"email\":\"roshan@gmail.com\",\"user_metadata\":{\"fname\":\"Roshan\",\"lname\":\"Labh\",\"role_id\":\"1\"}}'),
('5b96329872d4bb47f9a38dbf',	'Suraj',	'Kumar',	'suraj@test.com',	1,	'{\"_id\":\"5b96329872d4bb47f9a38dbf\",\"email_verified\":false,\"email\":\"suraj@test.com\",\"user_metadata\":{\"fname\":\"Suraj\",\"lname\":\"Kumar\",\"role_id\":\"1\"}}'),
('5b963b776a52865dc2e23e83',	'Mayank',	'Kanwar',	'mayank_kanwar@gmail.com',	1,	'{\"_id\":\"5b963b776a52865dc2e23e83\",\"email_verified\":false,\"email\":\"mayank_kanwar@gmail.com\",\"user_metadata\":{\"fname\":\"Mayank\",\"lname\":\"Kanwar\",\"role_id\":\"1\"}}');

-- 2018-09-10 09:38:59
